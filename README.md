# JavaScript Piggy Bank Demo

## About
A small application simulating a piggy bank.
Also contains an API call to fetch Drinks.

## Contributors
@teacher.warren.english

## Licence
©️ Noroff 2024
