let balance = 100 // stay hidden

function deposit(amount) {
    balance += amount
}

function emptyAccount() {
    balance = 0
}

function getBalance() {
    return balance
}

const bank = {
    deposit,
    emptyAccount,
    getBalance,
}

export default bank