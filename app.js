import bank from './bank.js'
import { DRINKS_API_URL } from './utils.js'

// DOM Elements
const btnDeposit = document.getElementById('btn-deposit')
const btnSmash = document.getElementById('btn-smash')
const displayBalance = document.getElementById('display-balance')
const selectDrinks = document.getElementById('select-drinks')
const selectedDrink = document.getElementById('selected-drink')

// Event Listeners
btnDeposit.addEventListener('click', handleDeposit)
btnSmash.addEventListener('click', handleSmash)
selectDrinks.addEventListener('change', handleDrinkSelected)

// Event Handlers
function handleDeposit() {
    console.log("fired handleDeposit()");
    const depositAmount = 100
    
    // call setter
    bank.deposit(depositAmount)
    
    // call get balance (getter)
    renderBankBalance()
}

function handleSmash() {
    console.log("fired handleSmash()");
    
    bank.emptyAccount()

    renderBankBalance()
}

function handleDrinkSelected(event) {
    console.log("fired handleDrinkSelected");

    // get selected drink
    console.log(event.target.value);

    // re-render selectedDrink

    currentSelectedDrink = drinks.find((item) => item.id == event.target.value)

    console.log(currentSelectedDrink);

    renderSelectedDrink()
}

// Variables
let drinks = []
let currentSelectedDrink = undefined

// Functions
function renderBankBalance() {
    // format the balance nicely
    displayBalance.innerText = `$${bank.getBalance()}`
}

function renderSelectedDrink() {
    // update selectedDrink to currently selected drink
    selectedDrink.innerText = `Selected drink: ${currentSelectedDrink.description}`
}

async function getDrinksFromAPI() {
    console.log("fired getDrinksFromAPI");
    const resp = await fetch(DRINKS_API_URL)
    const json = await resp.json()

    drinks = json

    // populate the dropdown list / select box
    populateSelectBox()
}

function populateSelectBox() {
    selectDrinks.innerHTML = ''

    for (const drink of drinks) {
        const newOption = document.createElement('option')
        newOption.innerText = `${drink.description} - ${drink.price}`
        newOption.value = drink.id

        selectDrinks.appendChild(newOption)
    }
}

// runtime
getDrinksFromAPI()